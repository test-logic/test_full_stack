/* 2-b. Bạn hãy viết 1 câu truy vấn để tính doanh thu mỗi tháng và 1 câu truy vấn lấy thương hiệu bán chạy nhất của mỗi tháng của năm hiện tại. (dựa vào database bạn vừa thiết kế) */

-- câu truy vấn để tính doanh thu mỗi tháng
SELECT 
  DATE_FORMAT(`updated_at`, '%m-%Y') AS `orders_month`, 
  SUM(`total_money`) as `revenue`, 
  COUNT(`order_id`) AS `count` 
FROM 
  `orders` 
WHERE 
  `status` = 1 
GROUP BY 
  `orders_month` 
ORDER BY 
  `orders_month` DESC


-- câu truy vấn lấy thương hiệu bán chạy nhất của mỗi tháng của năm hiện tại
SELECT 
  DATE_FORMAT(`or`.`updated_at`, '%m-%Y') AS `orders_month`, 
  `makers`.`maker_name`, 
  SUM(`or`.`quantity`) as `quantity` 
FROM 
  (
    SELECT 
      `orders_detail`.`product_id`, 
      `orders`.`status`, 
      `orders`.`updated_at`, 
      `orders_detail`.`quantity` 
    FROM 
      `orders` 
      INNER JOIN `orders_detail` ON `orders_detail`.`order_id` = `orders`.`order_id`
  ) as `or` 
  INNER JOIN `products` ON `products`.`product_id` = `or`.`product_id` 
  INNER JOIN `makers` ON `makers`.`maker_id` = `products`.`maker_id` 
WHERE 
  YEAR(`or`.`updated_at`) = YEAR(CURRENT_DATE()) 
  AND `or`.`status` = 1 
GROUP BY 
  `orders_month`, 
  `makers`.`maker_id` 
ORDER BY 
  `quantity`, 
  `orders_month` DESC;
