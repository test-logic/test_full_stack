<?php

    $html_string = '<ul><li class="item"><div class="image"><img src="https://webikevn-8743.kxcdn.com/static/wvn.05eb0c5430506f3.55707512.jpg" alt="EXCITER 135 57ZZ"></div><div class="bike-name"><a href="https://www.webike.vn/cho-xe-may/bike-detail/exciter-135-57-175-cc-1194114.html">Yamaha Exciter 2012 100 - 175 cc</a></div><div class="price">18.800.000 VNĐ</div><div class="seller-info"><div class="name">Nguyễn Văn A .<span>090.000.0000</span></div></div><div class="bike-info"><table><tbody><tr><td width="18%"><label title="">Dòng xe</label></td><td width="30%">YAMAHA Yamaha Exciter 150 Camo</td></tr><tr><td><label title="Đời xe">Đời xe</label></td><td>2012</td></tr><tr><td><label title="Phân khối">Phân khối</label></td><td>150cc</td></tr></tbody></table></div></li></ul>';

    $dom = new DOMDocument();

    // Load chuỗi HTML vào DOMDocument
    $dom->loadHTML($html_string);

    // Tạo một đối tượng DOMXPath
    $xpath = new DOMXPath($dom);

    // Tìm các phần tử li trong class container
    $items = $xpath->query('//li[@class="item"]');

    // In ra nội dung của các phần tử p tìm thấy
    foreach ($items as $item) {
        // Tìm thuộc tính src trong thẻ img bao bọc bởi phần tử li
        $img = $xpath->query('.//img//@src', $item)->item(0)->nodeValue;
        
        // Tìm thuộc tính href trong thẻ a bao bọc bởi phần tử li
        $href = $xpath->query('.//a//@href', $item)->item(0)->nodeValue;

        // Tìm thuộc tính class bike-name trong thẻ div bao bọc bởi phần tử li
        $bike_name = $xpath->query('.//div[@class="bike-name"]', $item)->item(0)->nodeValue;

        // Tìm thuộc tính class price trong thẻ div bao bọc bởi phần tử li
        $price = $xpath->query('.//div[@class="price"]', $item)->item(0)->nodeValue;
        
        // Tìm thuộc tính class seller-info trong thẻ div có thuộc tính name và lấy text bao bọc bởi phần tử li
        $seller_name = $xpath->query('.//div[@class="seller-info"]//div[@class="name"]//text()[normalize-space()]', $item)->item(0)->nodeValue;

        // Do có các ký tự đang là ISO-8859-1 nên buộc phải chuyển từ UTF-8 sang ISO-8859-1
        $seller_name = iconv(mb_detect_encoding($seller_name, mb_detect_order(), true), 'ISO-8859-1//TRANSLIT', $seller_name);
        // Xóa các special characters và giữ lại tiếng việt có dấu của VN 
        $seller_name = preg_replace('/[^a-z0-9A-Z_[:space:]ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ]/u','', $seller_name);
        // Nếu có từ 2 khoảng trắng thì đưa về 1 khoảng trắng
        $seller_name = preg_replace('/\s+/', ' ', $seller_name);
        // Clear các khoảng trắng ở đầu và cuối chuỗi
        $seller_name = trim($seller_name);

        // Tìm thuộc tính class seller-info trong thẻ div có thuộc tính name và lấy text bao bọc bởi phần tử li
        // normalize-space loại bỏ khoảng trắng ở đầu và cuối của một chuỗi, thay thế chuỗi ký tự khoảng trắng bằng một khoảng trắng và trả về chuỗi kết quả.
        $tel = $xpath->query('.//div[@class="seller-info"]//div[@class="name"]//text()[normalize-space()]', $item)->item(1)->nodeValue;
        $model = $xpath->query('.//div[@class="bike-info"]//td[2]//text()[normalize-space()]', $item)->item(0)->nodeValue;
        $year = $xpath->query('.//div[@class="bike-info"]//td[2]//text()[normalize-space()]', $item)->item(1)->nodeValue;
        $displacement = $xpath->query('.//div[@class="bike-info"]//td[2]//text()[normalize-space()]', $item)->item(2)->nodeValue;

        $arr[] = [
            'imgage' => $img,
            'href' => $href,
            'bike_name' => $bike_name,
            'price' => preg_replace('/[^0-9]/', '', $price), // chỉ lấy các ký tự số
            'seller_name' => $seller_name,
            'tel' => preg_replace('/[^0-9]/', '', $tel), // chỉ lấy các ký tự số
            'model' => $model,
            'year' => $year,
            'displacement' => $displacement
        ];
    }

    print_r($arr);