import { defineConfig, splitVendorChunkPlugin } from 'vite';
import laravel from 'laravel-vite-plugin';

import path from 'path';

export default defineConfig({
    plugins: [
        laravel({
            input: [
                'resources/assets/cms/css/app.css', 
                'resources/assets/cms/scss/app.scss',
                'resources/assets/cms/js/app.js',
                'resources/assets/cms/js/partials/navigationTop.js',
                'resources/assets/cms/js/partials/slideBar.js',
                'resources/assets/cms/js/livewire/tasks/list/index.js',
                'resources/assets/cms/js/livewire/tasks/list/create.js'
            ],
            refresh: true,
        }),
        splitVendorChunkPlugin(),
    ],
    resolve: {
        alias: {
            '@fortawesome': path.resolve(__dirname, 'node_modules/@fortawesome/fontawesome-free/js/all.js'),
            'moment': path.resolve(__dirname, 'node_modules/moment/moment.js'),
            'daterangepicker': path.resolve(__dirname, 'node_modules/daterangepicker'),
            // 'toastr': path.resolve(__dirname, 'node_modules/toastr'),
        }
    },
    build: {
        rollupOptions: {
            output: {
                // assetFileNames: "assets/[name]/[name][extname]",
                assetFileNames: (assetInfo) => {
                    // assetInfo.facadeModuleId contains the file's full path
                    let extType = assetInfo.name.split('.').at(1);
                    if (/png|jpe?g|svg|gif|tiff|bmp|ico/i.test(extType)) {
                        extType = 'images';
                    }
                    console.log(assetInfo.name);
                    return `assets/cms/${extType}/[hash][extname]`;
                },
                entryFileNames: (a) => {
                    return a.name.endsWith(".js") ? a.name : "assets/cms/js/[hash].js"
                },
                chunkFileNames: (a) => {
                    return a.name.endsWith(".js") ? a.name : "assets/cms/js/[hash].js"
                },
                manualChunks: (id) => {
                    if (id.includes('node_modules/@fortawesome')) {
                        return 'fortawesome';
                    }
                },
                experimentalMinChunkSize: 500,
            }
        },
    }
});
