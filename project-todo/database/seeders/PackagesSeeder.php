<?php

namespace Database\Seeders;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use DB;

class PackagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     */
    public function run(): void
    {
        //
        DB::table('packages')->upsert(
            [
                ['package_name' => 'free', 'package_displayname' => 'Free', 'price' => 0, 'unit' => '$', 'duration' => 0, 'limit_todo' => 10],
                ['package_name' => 'starter', 'package_displayname' => 'Starter', 'price' => 29, 'unit' => '$', 'duration' => 30, 'limit_todo' => 50],
                ['package_name' => 'professional', 'package_displayname' => 'Professional', 'price' => 99, 'unit' => '$', 'duration' => 30, 'limit_todo' => 500],
                ['package_name' => 'premium', 'package_displayname' => 'Premium', 'price' => 299, 'unit' => '$', 'duration' => 30, 'limit_todo' => -1],
            ],
            ['package_name'],
            ['package_displayname', 'price', 'unit', 'duration', 'limit_todo']
        );
    }
}
