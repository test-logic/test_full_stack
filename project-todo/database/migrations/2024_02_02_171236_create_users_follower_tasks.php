<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users_follower_tasks', function (Blueprint $table) {
            $table->id('users_follower_id');
            $table->unsignedBigInteger('user_id')->index();
            $table->unsignedBigInteger('task_id')->index();
            $table->timestamps();

            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('No Action');
            $table->foreign('task_id')->references('task_id')->on('tasks')->onDelete('No Action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users_follower_tasks');
    }
};
