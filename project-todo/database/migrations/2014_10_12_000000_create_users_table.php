<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id('user_id');
            $table->string('username')->unique()->index();
            $table->string('password')->index();
            $table->string('first_name')->index();
            $table->string('last_name')->index();
            $table->string('full_name')->index();
            $table->string('email')->index();
            $table->timestamp('email_verified_at')->nullable();
            // $table->unsignedBigInteger('package_id')->default(1)->index();
            $table->string('package_name')->default('free')->index();
            $table->dateTime('package_start_date')->nullable();
            $table->dateTime('package_end_date')->nullable();
            $table->integer('package_status')->comment('0: active,1: cancelled,2: expired')->default(1)->index();
            $table->dateTime('last_online')->index();
            $table->rememberToken();
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('users');
    }
};
