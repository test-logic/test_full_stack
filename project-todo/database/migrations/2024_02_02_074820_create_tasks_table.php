<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('tasks', function (Blueprint $table) {
            $table->id('task_id');
            $table->unsignedBigInteger('user_id');
            $table->string('task_name')->index();
            $table->longText('description');
            $table->integer('parent_id')->nullable();
            $table->integer('status')->comment('-1: Deleted; 0: Completed; 1: Doing; 2: Fail;')->index();
            $table->unsignedBigInteger('left_value')->comment('ID of task')->index();
            $table->unsignedBigInteger('right_value')->comment('ID of task')->index();
            $table->integer('level')->index();
            $table->unsignedBigInteger('created_by')->index();
            $table->timestamps();

            $table->foreign('user_id')->references('user_id')->on('users')->onDelete('No Action');
            $table->foreign('created_by')->references('user_id')->on('users')->onDelete('No Action');
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('tasks');
    }
};
