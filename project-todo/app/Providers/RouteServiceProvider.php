<?php

namespace App\Providers;

use Illuminate\Cache\RateLimiting\Limit;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\RateLimiter;
use Illuminate\Support\Facades\Route;
use Auth;

class RouteServiceProvider extends ServiceProvider
{
    /**
     * The path to your application's "home" route.
     *
     * Typically, users are redirected here after authentication.
     *
     * @var string
     */
    public const HOME = '/home';

    /**
     * Define your route model bindings, pattern filters, and other route configuration.
     */
    public function boot(): void
    {
        RateLimiter::for('api', function (Request $request) {
            return Limit::perMinute(60)->by($request->user()?->id ?: $request->ip());
        });

        // Route::bind('package', function (string $value) {
        //     // dd($value);
        //     $package = Auth::guard('cms')->user()->package_name;
        //     // $variableFromUrl = request()->route()->parameter('variable_name'); // Thay 'variable_name' bằng tên của biến trong URL

        //     // Chia sẻ biến với tất cả các view
        //     // view()->share('package', $package);
        //     // app()->instance('package', $package);
        //     // Livewire::component('*', function ($component) use ($package) {
        //     //     $component->package = $package;
        //     // });
    
        //     return $package;
        // });

        $this->routes(function () {
            Route::middleware('api')
                ->prefix('api')
                ->group(base_path('routes/api.php'));

            Route::middleware('web')
                ->group(base_path('routes/web.php'));
        });
    }
}
