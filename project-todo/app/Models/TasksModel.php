<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class TasksModel extends Model
{
    use HasFactory;

    protected $table = 'tasks';

    protected $primaryKey = 'task_id';

    protected $fillable = [
        'user_id',
        'task_name',
        'description',
        'parent_id',
        'status',
        'left_value',
        'right_value',
        'level',
        'created_by'
    ];
}
