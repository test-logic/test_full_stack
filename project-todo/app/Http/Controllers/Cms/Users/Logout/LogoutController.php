<?php

namespace App\Http\Controllers\Cms\Users\Logout;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;

class LogoutController extends Controller
{
    //
    public function index(Request $request)
    {
        if (Auth::guard('cms')->check())
        {
            Auth::guard('cms')->logout();

            $request->session()->invalidate();
 
            $request->session()->regenerateToken();
        }

        return redirect(route('cms.login'));
    }
}
