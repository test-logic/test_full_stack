<?php

namespace App\Http\Controllers\Cms\Users\Create;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Auth;
use Illuminate\Validation\Rule;
use App\Models\User;
use Carbon\Carbon;
use Exception, Log, DB;

class CreateController extends Controller
{
    //
    public function index()
    {
        return view('cms.users.create.index');
    }

    public function create(Request $request) : RedirectResponse
    {
        $validated = $request->validate([
            'first_name' => 'required',
            'last_name' => 'required',
            'full_name' => 'required',
            'email' => 'required|email',
            'username' => 'required|exclude_if:users,username',
            'password' => 'required',
            'confirn_password' => 'required|same:password'
        ]);

        DB::beginTransaction();
        try {
            $request->offsetSet('start_date', Carbon::now());
            $request->offsetSet('last_online', Carbon::now());
            User::create($request->all());

            DB::commit();
        } catch (Exception $ex) {
            DB::rollback();
            Log::channel('users-create')->info($ex->getMessage());
            return back();
        }
        
        return redirect()->route('cms.login');
    }
}
