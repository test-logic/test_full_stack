<?php

namespace App\Http\Controllers\Cms\Users\Login;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Http\RedirectResponse;
use Auth;
use Carbon\Carbon;

class LoginController extends Controller
{
    //
    public function index(Request $request)
    {
        return view('cms.users.login.index');
    }

    public function login(Request $request) : RedirectResponse
    {
        $credentials = $request->only('username', 'password');

        $remember = $request->has('remember');

        $message = 'Tài khoản hoặc mật khẩu không đúng!';
        if (Auth::guard('cms')->attempt($credentials, $remember)) {
            $request->session()->regenerate();
            $user = Auth::guard('cms')->user();

            if (Carbon::now()->diffInDays($user->last_online) > 3) {
                $message  = 'Tài khoản của bạn đã quá 3 ngày không truy cập';
                Auth::guard('cms')->logout();
            } else {
                $user->last_online = Carbon::now();
                $user->save();
                return redirect('cms');
            }
        }

        return back()->withErrors([
            'message' => $message,
        ])->onlyInput('username');
    }
}
