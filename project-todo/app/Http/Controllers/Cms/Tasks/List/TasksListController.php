<?php

namespace App\Http\Controllers\Cms\Tasks\List;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class TasksListController extends Controller
{
    //
    public function index() 
    {
        return view('cms.tasks.list.index');
    }
}
