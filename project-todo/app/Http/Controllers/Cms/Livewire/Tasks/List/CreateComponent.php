<?php

namespace App\Http\Controllers\Cms\Livewire\Tasks\List;

use Livewire\Component;
use Livewire\Attributes\Validate;
use App\Models\TasksModel;
use Auth;
use App\Models\PackagesModel;

class CreateComponent extends Component
{
    #[Validate('required|min:10')] 
    public $task_name;

    #[Validate('required|min:20')] 
    public $description;

    public function create()
    {
        $this->validate();

        $package = PackagesModel::where('package_name', Auth::guard('cms')->user()->package_name)->first();

        if ($package->limit_todo != -1) {
            $taskValidate = TasksModel::where('user_id', Auth::guard('cms')->user()->user_id)->limit($package->limit_todo + 1)->count();
            if ($taskValidate >= $package->limit_todo) {
                return $this->dispatch('justAlert', type: 'error', message: 'Tasks của bạn đã vượt hạn mức của gói cho phép!');
            }
        }
        
        TasksModel::create([
            'user_id' => Auth::guard('cms')->user()->user_id,
            'task_name' => $this->task_name,
            'description' => $this->description,
            'status' => 1,
            'left_value' => 1,
            'right_value' => 2,
            'level' => 1,
            'created_by' => Auth::guard('cms')->user()->user_id,
        ]);

        $this->dispatch('justAlert', type: 'success', message: 'Thành công!');
        $this->dispatch('closeModal', 'TasksCreate');
        $this->dispatch('refreshListComponent');

        $this->clearForm();
    }

    public function clearForm()
    {
        $this->task_name = null;
        $this->description = null;
    }

    public function render()
    {
        return view('cms.livewire.tasks.list.create-component');
    }
}
