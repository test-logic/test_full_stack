<?php

namespace App\Http\Controllers\Cms\Livewire\Tasks\List;

use Livewire\Component;
use StdClass;
use Carbon\Carbon;

class FilterComponent extends Component
{
    public $dateRange;
    
    public function mount()
    {
        $now = Carbon::now();
        $end = (clone $now)->format('Y-m-d');
        $start = (clone $now)->startOfDay()->format('Y-m-d');
        $this->dateRange = "$start - $end";
    }

    public function previewData() {

        // if (!$this->accountList) {
        //     $this->accountList = [
        //         21862
        //     ];
        // }
        
        $filterParams               = new StdClass();
        $filterParams->dateRange    = $this->dateRange;
        // $filterParams->accountList  = $this->accountList;
        // $filterParams->brandName    = $this->brandName;
        // $filterParams->packageList  = $this->packageList;
        // $filterParams->phoneSearch  = $this->phoneSearch;
        // $filterParams->showPhone    = $this->showPhone;
        // $filterParams->showFullData = $this->showFullData;
        // $filterParams->cmcSearch    = $this->cmcSearch;
        // $filterParams->cmcSubSearch = $this->cmcSubSearch;
        // $filterParams->codeSearch   = $this->codeSearch;
        // $filterParams->code2Search  = $this->code2Search;
        // $filterParams->limit        = self::LIMITDATAPREVIEW;
        // $filterParams->to           = Auth::guard('cms')->user()->email;

        $this->dispatch('postTasksReportList', $filterParams);
    }

    public function render()
    {
        return view('cms.livewire.tasks.list.filter-component');
    }
}
