<?php

namespace App\Http\Controllers\Cms\Livewire\Tasks\List;

use Livewire\Component;
use Livewire\Attributes\On;
use Livewire\WithPagination;
use App\Models\TasksModel;
use Carbon\Carbon;
use DB;

class ListComponent extends Component
{
    use WithPagination;

    protected $listeners = [
        'refreshListComponent' => '$refresh'
    ];

    public $filterParams;

    #[On('postTasksReportList')]
    public function updateTasksReportList($filterParams)
    {
        // ...
        $this->filterParams = $filterParams;
        $this->resetPage();
    }

    public function create()
    {
        $this->dispatch('openModalCreate');
    }

    public function edit($id)
    {
        $this->dispatch('postTasksEditLoadForm', $id);
        $this->dispatch('openModalEdit');
    }

    public static function getQuery($filterParams) {
        $tasks = null;
        $filterParams = (array) $filterParams;
        //Check phải có khoảng thời gian
        if ( $filterParams != null && $filterParams['dateRange'] ) {
            $period = $filterParams['dateRange'];

            if ($period) {
                $periodRange = explode(' - ', $period);
                $startDate = Carbon::parse($periodRange[0]);
                $endDate =  Carbon::parse($periodRange[1]);
                if ($startDate && $endDate) {
                    $tasks = TasksModel::select(
                        "task_id",
                        "user_id",
                        "task_name",
                        "description",
                        DB::raw("CASE
                            WHEN `status` = '0' THEN 'Completed'
                            WHEN `status` = '1' THEN 'Doing'
                            WHEN `status` = '2' THEN 'Fail'
                            END AS `status`"
                        ),
                        "created_by",
                        "created_at",
                    )
                    ->whereBetween('created_at', [$startDate->format('Y-m-d 00:00:00'), $endDate->format('Y-m-d 23:59:59')])
                    ->where('status', '<>', -1);
                }
            }
        }

        return $tasks;
    }

    public function render()
    {
        $filterParams = $this->filterParams;
        $query = self::getQuery($filterParams);

        if (!$query) $tasks = [];
        else {
            $tasks = $query->orderBy('created_at', 'desc')->paginate(20);
        }

        return view('cms.livewire.tasks.list.list-component', [
            'tasks' => $tasks
        ]);
    }
}
