<?php

namespace App\Http\Controllers\Cms\Livewire\Tasks\List;

use Livewire\Component;
use Livewire\Attributes\On;
use Livewire\Attributes\Validate;
use App\Models\TasksModel;

class EditComponent extends Component
{
    public $tasks_id;

    #[Validate('required|min:10')] 
    public $task_name;

    #[Validate('required|min:20')] 
    public $description;

    #[Validate('required|in:0,1,2')] 
    public $status;

    #[On('postTasksEditLoadForm')]
    public function loadFrom($tasks_id)
    {
        $task = TasksModel::where('task_id', $tasks_id)->first();
        $this->tasks_id = $tasks_id;
        $this->task_name = $task->task_name;
        $this->status = $task->status;
        $this->description = $task->description;
    }

    public function edit()
    {
        $this->validate();

        $task = TasksModel::where('task_id', $this->tasks_id)->first();

        $task->task_name = $this->task_name;
        $task->description = $this->description;
        $task->status = $this->status;
        $task->save();

        $this->dispatch('justAlert', type: 'success', message: 'Thành công!');
        $this->dispatch('closeModal', 'TasksEdit');
        $this->dispatch('refreshListComponent');
    }

    public function render()
    {
        return view('cms.livewire.tasks.list.edit-component');
    }
}
