<?php

namespace App\Http\Controllers\Cms\Livewire\Partials;

use Livewire\Component;
use App\Models\Subsription;
use Auth;
use Carbon\Carbon;

class NavigationTop extends Component
{
    public $package;

    public function mount()
    {
    }

    public function cancelPackage()
    {
        $package = Auth::guard('cms')->user()->package_name;
        if ($package == 'free')
        {
            return $this->dispatch('justAlert', type: 'error', message: 'Package của bạn là Free!');
        }

        $subsription = Auth::guard('cms')->user();

        if ($subsription->package_end_date > Carbon::now()) {
            $subsription->package_status = 1;
        } else {
            $subsription->package_name = 'free';
            $subsription->package_end_date = Carbon::now();
            $subsription->package_status = 0;
        }

        $subsription->save();

        return $this->dispatch('justAlert', type: 'success', message: 'Hủy thành công!');
    }

    public function render()
    {
        return view('cms.livewire.partials.navigation-top');
    }
}
