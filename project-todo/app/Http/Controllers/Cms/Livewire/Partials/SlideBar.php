<?php

namespace App\Http\Controllers\Cms\Livewire\Partials;

use Livewire\Component;
use Livewire\Attributes\On;
use URL;
use App\Models\PackagesModel;
use App\Models\Subsription;
use Auth;
use Carbon\Carbon;

class SlideBar extends Component
{
    public $packageList = [];
    public $package;

    public function mount()
    {
        $this->package = $package = Auth::guard('cms')->user()->package_name;
        $this->packageList = PackagesModel::where('package_name', '<>', $package)->get();
    }

    #[On('buyPackage')]
    public function buyPackage($package_name)
    {
        $package = $this->packageList->where('package_name', $package_name)->first();
        if (!$package) {
            return $this->dispatch('justAlert', type: 'error', message: 'Package không tồn tại');
        }

        $subsription = Auth::guard('cms')->user();

        $subsription->package_name = $package_name;
        $subsription->package_status = 0;
        $subsription->package_start_date = Carbon::now();
        $subsription->package_end_date = Carbon::now()->addDays($package->duration);

        $subsription->save();

        $this->dispatch('justAlert', type: 'success', message: 'Thành công!');

        return redirect()->route('cms.home', ['package' => $package->package_name]);
    }

    public function render()
    {
        return view('cms.livewire.partials.slide-bar', [
            'current_url' => URL::current(),
        ]);
    }
}
