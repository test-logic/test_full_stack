<?php

namespace App\Http\Controllers\Cms\Livewire\Partials;

use Livewire\Component;

class Loading extends Component
{
    public function render()
    {
        return view('cms.livewire.partials.loading');
    }
}
