<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;
use App\Models\Subsription;
use Auth;

class AuthenticatePackages
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next): Response
    {
        // dd(Auth::guard('cms')->user(), 123);

        $package = Auth::guard('cms')->user()->package_name;
        if ($package) {
            return redirect()->route('cms.home', ['package' => $package]);
        }
        
        return $next($request);
    }
}
