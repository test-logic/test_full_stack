<?php

namespace App\Http\Middleware;

use App\Providers\RouteServiceProvider;
use Closure;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Symfony\Component\HttpFoundation\Response;
use Carbon\Carbon;

class RedirectIfAuthenticated
{
    /**
     * Handle an incoming request.
     *
     * @param  \Closure(\Illuminate\Http\Request): (\Symfony\Component\HttpFoundation\Response)  $next
     */
    public function handle(Request $request, Closure $next, string ...$guards): Response
    {
        $guards = empty($guards) ? [null] : $guards;

        foreach ($guards as $guard) {
            if ($guard == 'cms') {
                if (Auth::guard($guard)->check()) {
                    $user = Auth::guard($guard)->user();

                    if (Carbon::now()->diffInDays($user->last_online) < 3) {
                        $user->last_online = Carbon::now();
                        $user->save();
                    } else {
                        Auth::guard($guard)->logout();

                        $request->session()->invalidate();
 
                        $request->session()->regenerateToken();

                        // $this->redirect('/cms');
                        return redirect()->route('cms.login');
                    }
                } else {
                    return redirect()->guest(route('cms.login'));
                }
            }
        }

        return $next($request);
    }
}
