@extends('app')

@section('title')
    @yield('cms.title') | CMS Syngenta
@endsection

@push('css')
    {{-- <link rel="icon" href="{{ Vite::asset('resources/cms/images/icons/favicon.ico') }}" type="image/vnd.microsoft.icon"> --}}
    @livewireStyles
    @vite(['resources/assets/cms/css/app.css', 'resources/assets/cms/scss/app.scss'])
    @stack('cms.css')
@endpush

@section('content')
    <body class="bg-gray-50">
        @livewire('partials.loading', ['lazy' => true])
        <noscript>
            <iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THQTXJ7" height="0" width="0" style="display: none; visibility: hidden"></iframe>
        </noscript>
        @yield('cms.navigationTop')
        <div class="flex overflow-hidden bg-white pt-14">
            @yield('cms.slideBar')

            <div class="hidden fixed inset-0 z-10 bg-gray-900" id="sidebarBackdrop"></div>
                <div id="main-content" class="h-full w-full bg-gray-50 relative overflow-y-auto lg:ml-64">
                    <div class="grid grid-cols-1 px-4 pt-6 xl:grid-cols-3 xl:gap-6">
                        <div class="col-span-full xl:mb-0">
                            {{-- <nav class="flex mb-5" aria-label="Breadcrumb">
                                <ol class="inline-flex items-center space-x-1 md:space-x-2">
                                <li class="inline-flex items-center">
                                    <a href="#" class="inline-flex items-center text-gray-700 hover:text-gray-900">
                                    <svg class="w-5 h-5 mr-2.5" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
                                    </svg> Home </a>
                                </li>
                                <li>
                                    <div class="flex items-center">
                                        <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                            <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
                                        </svg>
                                        <a href="#" class="ml-1 text-sm font-medium text-gray-700 hover:text-gray-900 md:ml-2">Users</a>
                                    </div>
                                </li>
                                <li>
                                    <div class="flex items-center">
                                    <svg class="w-6 h-6 text-gray-400" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                        <path fill-rule="evenodd" d="M7.293 14.707a1 1 0 010-1.414L10.586 10 7.293 6.707a1 1 0 011.414-1.414l4 4a1 1 0 010 1.414l-4 4a1 1 0 01-1.414 0z" clip-rule="evenodd"></path>
                                    </svg>
                                    <span class="ml-1 text-sm font-medium text-gray-400 md:ml-2" aria-current="page">Settings</span>
                                    </div>
                                </li>
                                </ol>
                            </nav> --}}
                            @if (isset($compact))
                                {!! Helpers::getVal($compact, 'breadcrumb') !!}
                            @endif
                        </div>
                    </div>
                    {{-- <div class="grid grid-cols-1 px-1 xl:grid-cols-3 xl:gap-6"> --}}
                    @yield('cms.content')
                    {{-- </div> --}}
                </div>
                
                <svg id="SvgjsSvg1001" width="2" height="0" xmlns="http://www.w3.org/2000/svg" version="1.1" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svgjs="http://svgjs.dev" style="overflow: hidden; top: -100%; left: -100%; position: absolute; opacity: 0;">
                    <defs id="SvgjsDefs1002"></defs>
                    <polyline id="SvgjsPolyline1003" points="0,0"></polyline>
                    <path id="SvgjsPath1004" d="M0 0 "></path>
                </svg>
            </div>
        </div>
        
        @push('js')
            @livewireScriptConfig
            @vite(['resources/assets/cms/js/app.js'])
            @stack('cms.js')
        @endpush
    </body>
@endsection


