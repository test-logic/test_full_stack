@extends('cms.masterLayout')

@section('cms.title')
    Home
@endsection

@section('cms.navigationTop')
    @livewire('partials.navigation-top')
@endsection

@section('cms.slideBar')
    @livewire('partials.slide-bar')
@endsection

@section('cms.content')
<div class="h-full w-full bg-gray-50 relative overflow-y-auto lg:ml-64">
    
</div>
@endsection