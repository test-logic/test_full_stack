<aside id="sidebar" class="flex hidden fixed top-0 left-0 z-20 flex-col flex-shrink-0 h-full lg:duration-200 lg:flex transition-width w-64" aria-label="Sidebar">
    <div class="flex relative flex-col flex-1 pt-0 bg-gray-50 overflow-y-auto min-h-0">
        <div class="flex flex-col flex-1">
            <div class="flex-1 bg-gray-50 pl-3" id="sidebar-items">
                <ul class="pb-2 pt-1">
                    <li>
                        <a href="https://demos.creative-tim.com/soft-ui-flowbite-pro/" class="flex items-center p-2 text-base font-normal text-dark-500 rounded-lg hover:bg-gray-200 group transition-all duration-200 bg-gray-50 shadow-none" sidebar-toggle-collapse="">
                            <div class="bg-white shadow-lg shadow-gray-300 text-dark-700 w-8 h-8 p-2.5 mr-1 rounded-lg text-center grid place-items-center">
                                <svg width="12px" height="12px" viewBox="0 0 45 40" version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink">
                                    <title>shop</title>
                                    <g stroke="none" stroke-width="1" fill="none" fill-rule="evenodd">
                                        <g transform="translate(-1716.000000, -439.000000)" fill="currentColor" fill-rule="nonzero">
                                            <g transform="translate(1716.000000, 291.000000)">
                                                <g transform="translate(0.000000, 148.000000)">
                                                    <path class="color-background" d="M46.7199583,10.7414583 L40.8449583,0.949791667 C40.4909749,0.360605034 39.8540131,0 39.1666667,0 L7.83333333,0 C7.1459869,0 6.50902508,0.360605034 6.15504167,0.949791667 L0.280041667,10.7414583 C0.0969176761,11.0460037 -1.23209662e-05,11.3946378 -1.23209662e-05,11.75 C-0.00758042603,16.0663731 3.48367543,19.5725301 7.80004167,19.5833333 L7.81570833,19.5833333 C9.75003686,19.5882688 11.6168794,18.8726691 13.0522917,17.5760417 C16.0171492,20.2556967 20.5292675,20.2556967 23.494125,17.5760417 C26.4604562,20.2616016 30.9794188,20.2616016 33.94575,17.5760417 C36.2421905,19.6477597 39.5441143,20.1708521 42.3684437,18.9103691 C45.1927731,17.649886 47.0084685,14.8428276 47.0000295,11.75 C47.0000295,11.3946378 46.9030823,11.0460037 46.7199583,10.7414583 Z" opacity="0.598981585"></path>
                                                    <path class="color-background" d="M39.198,22.4912623 C37.3776246,22.4928106 35.5817531,22.0149171 33.951625,21.0951667 L33.92225,21.1107282 C31.1430221,22.6838032 27.9255001,22.9318916 24.9844167,21.7998837 C24.4750389,21.605469 23.9777983,21.3722567 23.4960833,21.1018359 L23.4745417,21.1129513 C20.6961809,22.6871153 17.4786145,22.9344611 14.5386667,21.7998837 C14.029926,21.6054643 13.533337,21.3722507 13.0522917,21.1018359 C11.4250962,22.0190609 9.63246555,22.4947009 7.81570833,22.4912623 C7.16510551,22.4842162 6.51607673,22.4173045 5.875,22.2911849 L5.875,44.7220845 C5.875,45.9498589 6.7517757,46.9451667 7.83333333,46.9451667 L19.5833333,46.9451667 L19.5833333,33.6066734 L27.4166667,33.6066734 L27.4166667,46.9451667 L39.1666667,46.9451667 C40.2482243,46.9451667 41.125,45.9498589 41.125,44.7220845 L41.125,22.2822926 C40.4887822,22.4116582 39.8442868,22.4815492 39.198,22.4912623 Z"></path>
                                                </g>
                                            </g>
                                        </g>
                                    </g>
                                </svg>
                            </div>
                            <span class="ml-3 text-dark-500 text-sm font-light" sidebar-toggle-item="">Dashboard</span>
                        </a>
                    </li>
                    
                </ul>
                <ul class="pb-2 pt-1" sidebar-toggle-list="">
                    <li>
                       <div class="rounded-lg @if ($current_url == route('cms.home', ['package' => Auth::guard('cms')->user()->package_name])) bg-white @endif shadow-gray-200 hover:bg-gray-200 group transition-all duration-200">
                          <a href="{{ route('cms.home', ['package' => Auth::guard('cms')->user()->package_name]) }}" class="w-full flex items-center p-2 text-base font-normal text-dark-500 rounded-lg hover:bg-gray-200 shadow-gray-200">
                                <div class=" shadow-lg shadow-gray-300 text-dark-700 w-8 h-8 p-2.5 mr-1 rounded-lg text-center grid place-items-center" sidebar-toggle-item="">
                                    <svg class="text-dark mb-1" width="12px" height="12px" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
                                    </svg>
                                </div>
                                <span class="ml-1 text-dark-500 text-sm font-light">Home</span>
                          </a>
                       </div>
                    </li>
                    <li>
                        <div class="rounded-lg @if ($current_url == route('cms.list', ['package' => Auth::guard('cms')->user()->package_name])) bg-white @endif shadow-gray-200 hover:bg-gray-200 group transition-all duration-200">
                           <a href="{{ route('cms.list', ['package' => Auth::guard('cms')->user()->package_name]) }}" class="w-full  flex items-center p-2 text-base font-normal text-dark-500 rounded-lg hover:bg-gray-200 shadow-gray-200">
                              <div class="shadow-lg shadow-gray-300 text-dark-700 w-8 h-8 p-2.5 mr-1 rounded-lg text-center grid place-items-center" sidebar-toggle-item="">
                                 <svg class="text-dark mb-1" width="12px" height="12px" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path d="M10.707 2.293a1 1 0 00-1.414 0l-7 7a1 1 0 001.414 1.414L4 10.414V17a1 1 0 001 1h2a1 1 0 001-1v-2a1 1 0 011-1h2a1 1 0 011 1v2a1 1 0 001 1h2a1 1 0 001-1v-6.586l.293.293a1 1 0 001.414-1.414l-7-7z"></path>
                                 </svg>
                              </div>
                              <span class="ml-1 text-dark-500 text-sm font-light">Tasks</span>
                           </a>
                        </div>
                     </li>
                </ul>
            </div>
        </div>
        <div class="hidden relative bottom-0 left-0 justify-center w-full lg:flex bg-gray-100 space-x-4 p-4" sidebar-bottom-menu="">
            <a href="#" class="inline-flex justify-center p-2 text-gray-500 rounded cursor-pointer hover:text-dark-500 hover:bg-gray-200">
            <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                <path d="M5 4a1 1 0 00-2 0v7.268a2 2 0 000 3.464V16a1 1 0 102 0v-1.268a2 2 0 000-3.464V4zM11 4a1 1 0 10-2 0v1.268a2 2 0 000 3.464V16a1 1 0 102 0V8.732a2 2 0 000-3.464V4zM16 3a1 1 0 011 1v7.268a2 2 0 010 3.464V16a1 1 0 11-2 0v-1.268a2 2 0 010-3.464V4a1 1 0 011-1z"></path>
            </svg>
            </a>
            <a href="#" data-te-collapse-init="" data-te-ripple-init="" data-te-ripple-color="light" data-te-target="#tooltip-settings" aria-expanded="false" aria-controls="collapseExample" data-dropdown-toggle="tooltip-settings" class="inline-flex justify-center p-2 text-gray-500 rounded cursor-pointer hover:text-dark-500 hover:bg-gray-200">
                <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                    <path fill-rule="evenodd" d="M11.49 3.17c-.38-1.56-2.6-1.56-2.98 0a1.532 1.532 0 01-2.286.948c-1.372-.836-2.942.734-2.106 2.106.54.886.061 2.042-.947 2.287-1.561.379-1.561 2.6 0 2.978a1.532 1.532 0 01.947 2.287c-.836 1.372.734 2.942 2.106 2.106a1.532 1.532 0 012.287.947c.379 1.561 2.6 1.561 2.978 0a1.533 1.533 0 012.287-.947c1.372.836 2.942-.734 2.106-2.106a1.533 1.533 0 01.947-2.287c1.561-.379 1.561-2.6 0-2.978a1.532 1.532 0 01-.947-2.287c.836-1.372-.734-2.942-2.106-2.106a1.532 1.532 0 01-2.287-.947zM10 13a3 3 0 100-6 3 3 0 000 6z" clip-rule="evenodd"></path>
                </svg>
            </a>
            <div class="z-50 hidden w-max text-base list-none bg-white rounded divide-y divide-gray-100 shadow-lg absolute left-1/2 transform -translate-x-1/2 m-0 -translate-y-full" id="tooltip-settings" data-popper-placement="bottom">
                <ul class="py-1" role="none">
                    @foreach($packageList as $key => $value)
                        <li>
                            <a class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-200 buyPackage" data-package="{{ $value->package_name }}" role="menuitem">
                                <div class="inline-flex items-center">
                                    <div class="mr-1">{!! MONEY_BILL_ICON !!}</div> {{ $value->package_displayname }} (Price: {{ $value->unit }}{{ $value->price }}) {{ $value->duration }} todo
                                </div>
                            </a>
                        </li>
                    @endforeach
                </ul>
            </div>
            <div class="relative grid grid-cols-1 gap-1">
                <button type="button" data-te-collapse-init="" data-te-ripple-init="" data-te-ripple-color="light" data-te-target="#language-dropdown" aria-expanded="false" aria-controls="collapseExample" data-dropdown-toggle="language-dropdown" class="inline-flex justify-center p-2 text-gray-500 rounded cursor-pointer hover:text-dark-500 hover:bg-gray-200">
                    @if ( config('app.locale') == 'vi' )
                        <img
                            src="{{ Vite::asset('resources/cms/images/icons/vietnam_flag.png') }}"
                            class="h-5 w-5 mt-0.5 object-contain" alt="Creative Tim Logo">
                    @elseif ( config('app.locale') == 'en' )
                        <svg class="h-5 w-5 rounded-full mt-0.5" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" viewBox="0 0 3900 3900">
                            <path fill="#b22234" d="M0 0h7410v3900H0z"></path>
                            <path d="M0 450h7410m0 600H0m0 600h7410m0 600H0m0 600h7410m0 600H0" stroke="#fff" stroke-width="300"></path>
                            <path fill="#3c3b6e" d="M0 0h2964v2100H0z"></path>
                            <g fill="#fff">
                            <g id="d">
                                <g id="c">
                                <g id="e">
                                    <g id="b">
                                    <path id="a" d="M247 90l70.534 217.082-184.66-134.164h228.253L176.466 307.082z"></path>
                                    <use xlink:href="#a" y="420"></use>
                                    <use xlink:href="#a" y="840"></use>
                                    <use xlink:href="#a" y="1260"></use>
                                    </g>
                                    <use xlink:href="#a" y="1680"></use>
                                </g>
                                <use xlink:href="#b" x="247" y="210"></use>
                                </g>
                                <use xlink:href="#c" x="494"></use>
                            </g>
                            <use xlink:href="#d" x="988"></use>
                            <use xlink:href="#c" x="1976"></use>
                            <use xlink:href="#e" x="2470"></use>
                            </g>
                        </svg>
                    @endif
                </button>
                <div class="z-50 hidden w-max text-base list-none bg-white rounded divide-y divide-gray-100 shadow-lg absolute left-1/2 transform -translate-x-1/2 m-0 -translate-y-full" id="language-dropdown" data-popper-placement="bottom">
                    <ul class="py-1" role="none">
                        {{-- <li>
                            <a href="{{  route('cms.change-language', ['language' => 'en'])  }}" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-200" role="menuitem">
                                <div class="inline-flex items-center">
                                <svg class="h-3.5 w-3.5 rounded-full mr-2" xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-us" viewBox="0 0 512 512">
                                    <g fill-rule="evenodd">
                                    <g stroke-width="1pt">
                                        <path fill="#bd3d44" d="M0 0h247v10H0zm0 20h247v10H0zm0 20h247v10H0zm0 20h247v10H0zm0 20h247v10H0zm0 20h247v10H0zm0 20h247v10H0z" transform="scale(3.9385)"></path>
                                        <path fill="#fff" d="M0 10h247v10H0zm0 20h247v10H0zm0 20h247v10H0zm0 20h247v10H0zm0 20h247v10H0zm0 20h247v10H0z" transform="scale(3.9385)"></path>
                                    </g>
                                    <path fill="#192f5d" d="M0 0h98.8v70H0z" transform="scale(3.9385)"></path>
                                    <path fill="#fff" d="M8.2 3l1 2.8H12L9.7 7.5l.9 2.7-2.4-1.7L6 10.2l.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8H45l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.4 1.7 1 2.7L74 8.5l-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9L92 7.5l1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm-74.1 7l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7H65zm16.4 0l1 2.8H86l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm-74 7l.8 2.8h3l-2.4 1.7.9 2.7-2.4-1.7L6 24.2l.9-2.7-2.4-1.7h3zm16.4 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8H45l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9L92 21.5l1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm-74.1 7l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7H65zm16.4 0l1 2.8H86l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm-74 7l.8 2.8h3l-2.4 1.7.9 2.7-2.4-1.7L6 38.2l.9-2.7-2.4-1.7h3zm16.4 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8H45l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9L92 35.5l1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm-74.1 7l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7H65zm16.4 0l1 2.8H86l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm-74 7l.8 2.8h3l-2.4 1.7.9 2.7-2.4-1.7L6 52.2l.9-2.7-2.4-1.7h3zm16.4 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8H45l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9L92 49.5l1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm-74.1 7l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7H65zm16.4 0l1 2.8H86l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm-74 7l.8 2.8h3l-2.4 1.7.9 2.7-2.4-1.7L6 66.2l.9-2.7-2.4-1.7h3zm16.4 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8H45l-2.4 1.7 1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9zm16.4 0l1 2.8h2.8l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h3zm16.5 0l.9 2.8h2.9l-2.3 1.7.9 2.7-2.4-1.7-2.3 1.7.9-2.7-2.4-1.7h2.9zm16.5 0l.9 2.8h2.9L92 63.5l1 2.7-2.4-1.7-2.4 1.7 1-2.7-2.4-1.7h2.9z" transform="scale(3.9385)"></path>
                                    </g>
                                </svg> English (US)
                                </div>
                            </a>
                        </li> --}}
                        {{-- <li>
                            <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-200" role="menuitem">
                                <div class="inline-flex items-center">
                                <svg class="h-3.5 w-3.5 rounded-full mr-2" xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-de" viewBox="0 0 512 512">
                                    <path fill="#ffce00" d="M0 341.3h512V512H0z"></path>
                                    <path d="M0 0h512v170.7H0z"></path>
                                    <path fill="#d00" d="M0 170.7h512v170.6H0z"></path>
                                </svg> Deutsch
                                </div>
                            </a>
                        </li>
                        <li>
                            <a href="#" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-200" role="menuitem">
                                <div class="inline-flex items-center">
                                <svg class="h-3.5 w-3.5 rounded-full mr-2" xmlns="http://www.w3.org/2000/svg" id="flag-icon-css-it" viewBox="0 0 512 512">
                                    <g fill-rule="evenodd" stroke-width="1pt">
                                        <path fill="#fff" d="M0 0h512v512H0z"></path>
                                        <path fill="#009246" d="M0 0h170.7v512H0z"></path>
                                        <path fill="#ce2b37" d="M341.3 0H512v512H341.3z"></path>
                                    </g>
                                </svg> Italiano
                                </div>
                            </a>
                        </li>
                        <li> --}}
                        {{-- <a href="{{  route('cms.change-language', ['language' => 'vi'])  }}" class="block py-2 px-4 text-sm text-gray-700 hover:bg-gray-200" role="menuitem">
                            <div class="inline-flex items-center">
                                <img
                                    data-te-lazy-load-init
                                    data-te-lazy-src="{{ Vite::asset('resources/cms/images/icons/vietnam_flag.png') }}"
                                    data-te-lazy-placeholder="{{ Vite::asset('resources/cms/images/icons/loading.gif') }}"
                                    src="{{ Vite::asset('resources/cms/images/icons/vietnam_flag.png') }}"
                                    class="h-3.5 w-3.5 mr-2 object-contain" alt="Creative Tim Logo">
                                Tiếng Việt
                            </div>
                        </a> --}}
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</aside>
@push('cms.js')
@vite(['resources/assets/cms/js/partials/slideBar.js'])
@endpush