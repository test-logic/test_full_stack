<div>
    {{-- Stop trying to control. --}}
    <nav class="fixed z-30 w-full bg-gray-50">
        <div class="py-3 px-3 lg:px-5 lg:pl-3">
            <div class="flex justify-between items-center">
                <div class="flex justify-start items-center">
                    <button id="toggleSidebar" aria-expanded="true" aria-controls="sidebar" class="hidden p-2 mr-4 text-gray-600 rounded cursor-pointer lg:inline hover:text-gray-900 hover:bg-gray-100">
                        {{-- <svg class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h6a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path>
                        </svg> --}}
                        <svg xmlns="http://www.w3.org/2000/svg" height="1em" viewBox="0 0 448 512">
                            <path d="M0 96C0 78.3 14.3 64 32 64H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32C14.3 128 0 113.7 0 96zM0 256c0-17.7 14.3-32 32-32H416c17.7 0 32 14.3 32 32s-14.3 32-32 32H32c-17.7 0-32-14.3-32-32zM448 416c0 17.7-14.3 32-32 32H32c-17.7 0-32-14.3-32-32s14.3-32 32-32H416c17.7 0 32 14.3 32 32z"/>
                        </svg>
                    </button>
                    <button id="toggleSidebarMobile" aria-expanded="true" aria-controls="sidebar" class="p-2 mr-2 text-gray-600 rounded cursor-pointer lg:hidden hover:text-gray-900 hover:bg-gray-100 focus:bg-gray-100 focus:ring-2 focus:ring-gray-100">
                        <svg id="toggleSidebarMobileHamburger" class="w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M3 5a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1zM3 10a1 1 0 011-1h6a1 1 0 110 2H4a1 1 0 01-1-1zM3 15a1 1 0 011-1h12a1 1 0 110 2H4a1 1 0 01-1-1z" clip-rule="evenodd"></path>
                        </svg>
                        <svg id="toggleSidebarMobileClose" class="hidden w-6 h-6" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                            <path fill-rule="evenodd" d="M4.293 4.293a1 1 0 011.414 0L10 8.586l4.293-4.293a1 1 0 111.414 1.414L11.414 10l4.293 4.293a1 1 0 01-1.414 1.414L10 11.414l-4.293 4.293a1 1 0 01-1.414-1.414L8.586 10 4.293 5.707a1 1 0 010-1.414z" clip-rule="evenodd"></path>
                        </svg>
                    </button>
                    <p class="text-md font-semibold items-center" id="loading-logo">
                        {{-- <img data-te-lazy-load-init
                            data-te-lazy-src="{{ Vite::asset('resources/cms/images/icons/logo.svg') }}"
                            data-te-lazy-placeholder="{{ Vite::asset('resources/cms/images/icons/loading.gif') }}"
                            class="h-8 object-contain" alt="Creative Tim Logo"> --}}
                        {{-- <span class="country-indicator tw-relative text-xs tw-text-gray-500 tw-leading-3 float-right">Việt Nam</span> --}}
                    </p>
                </div>
                <div class="flex items-center">
                    <div class="ml-3 relative">
                        <div>
                            <button type="button" class="flex items-center text-sm rounded-full focus:ring-4 focus:ring-gray-300" id="user-menu-button-2" aria-expanded="false" data-dropdown-toggle="dropdown-2">
                                <svg class="w-8 h-8 text-gray-500" fill="currentColor" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg">
                                    <path fill-rule="evenodd" d="M18 10a8 8 0 11-16 0 8 8 0 0116 0zm-6-3a2 2 0 11-4 0 2 2 0 014 0zm-2 4a5 5 0 00-4.546 2.916A5.986 5.986 0 0010 16a5.986 5.986 0 004.546-2.084A5 5 0 0010 11z" clip-rule="evenodd"></path>
                                </svg>
                                <span class="text-green-600 ml-1">{{ Auth::guard('cms')->user()->username }} ({{ Auth::guard('cms')->user()->user_id }})</span>
                            </button>
                            
                        </div>
                        <div class="hidden w-max z-50 my-4 text-base list-none bg-white rounded divide-y divide-gray-100 shadow-lg shadow-gray-300 right-0 absolute" id="dropdown-2" data-popper-placement="bottom">
                            {{-- <div class="py-3 px-4" role="none">
                                <p class="text-sm" role="none">Neil Sims</p>
                                <p class="text-sm font-medium text-gray-900 truncate" role="none"> neil.sims@flowbite.com </p>
                            </div> --}}
                            <ul class="py-1" role="none">
                                <li>
                                    <a wire:click="cancelPackage" class="block flex py-2 px-4 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                                        <div class="w-5 h-5 text-gray-500">{!! XMARK_ICON !!}</div>
                                        <span class="ml-1">Cancel Package</span>
                                    </a>
                                </li>
                                <li class="border-t border-t-gray-200">
                                    <a href="{{ route('cms.logout', ['package' => Auth::guard('cms')->user()->package_name]) }}" class="block flex py-2 px-4 text-sm text-gray-700 hover:bg-gray-100" role="menuitem">
                                        <svg class="w-5 h-5 text-gray-500" fill="none" stroke="currentColor" viewBox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                                            <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M11 16l-4-4m0 0l4-4m-4 4h14m-5 4v1a3 3 0 01-3 3H6a3 3 0 01-3-3V7a3 3 0 013-3h7a3 3 0 013 3v1"></path>
                                        </svg>
                                        <span class="ml-1">{{ __('Logout') }}</span>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </nav>
</div>
@push('cms.js')
@vite(['resources/assets/cms/js/partials/navigationTop.js'])
@endpush