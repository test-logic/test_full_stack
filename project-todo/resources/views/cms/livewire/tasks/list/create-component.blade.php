<div>
    
    {{-- Knowing others is intelligence; knowing yourself is true wisdom. --}}
    <!-- Modal -->
    <div
        wire:ignore.self
        {{-- data-te-modal-init --}}
        class="fixed left-0 top-0 z-[1055] hidden h-full w-full overflow-y-auto overflow-x-hidden outline-none"
        id="TasksCreate"
        {{-- data-te-backdrop="static"
        data-te-keyboard="false"
        tabindex="-1"
        aria-labelledby="staticBackdropLabel" --}}
        aria-hidden="true">
        <div
            wire:ignore.self
            data-te-modal-dialog-ref
            class="pointer-events-none opacity-0 relative translate-y-[-50px] transition-all duration-300 ease-in-out w-6/12 left-1/2 top-1/2 -translate-x-1/2 -translate-y-1/2 transform" style="
            left: 50%;
            top: 50%;
            transform: translate(-50%, -50%);">
            <div
                class="min-[576px]:shadow-[0_0.5rem_1rem_rgba(#000, 0.15)] pointer-events-auto relative flex w-full flex-col rounded-md border-none bg-white bg-clip-padding text-current shadow-lg outline-none">
                <div
                class="flex flex-shrink-0 items-center justify-between rounded-t-md border-b-2 border-neutral-300 border-opacity-100 p-4 dark:border-opacity-50">
                <!--Modal title-->
                <h3
                    class="text-xl font-semibold text-gray-900"
                    id="staticBackdropLabel">
                    Create task
                </h3>
                <!--Close button-->
                <button
                    type="button"
                    class="box-content rounded-none border-none hover:no-underline hover:opacity-75 focus:opacity-100 focus:shadow-none focus:outline-none"
                    data-te-modal-dismiss
                    data-te-toggle="modal"
                    data-te-target="#TasksCreate"
                    ata-te-ripple-init 
                    data-te-ripple-color="light"
                    aria-label="Close">
                    <svg
                        xmlns="http://www.w3.org/2000/svg"
                        fill="none"
                        viewBox="0 0 24 24"
                        stroke-width="1.5"
                        stroke="currentColor"
                        class="h-6 w-6">
                    <path
                        stroke-linecap="round"
                        stroke-linejoin="round"
                        d="M6 18L18 6M6 6l12 12" />
                    </svg>
                </button>
                </div>

                <!--Modal body-->
                <div data-te-modal-body-ref class="relative p-10">
                    <form wire:submit="create" autocomplete="off">
                        <div class="grid grid-cols-1 gap-1">
                            <div>
                                <label for="task_name" class="block mb-2 text-sm font-medium text-gray-900">Task Name:</label>
                                <input type="text" wire:model="task_name" id="task_name" value="" class="border border-gray-300  text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Task Name">
                                @error('task_name')
                                    <div class="text-red-600 mt-2">{{ $message }}</div>
                                @enderror
                            </div>
                            <div>
                                <label for="description" class="block mb-2 text-sm font-medium text-gray-900">Description:</label>
                                <textarea type="text" wire:model="description" id="description" value="" class="border border-gray-300  text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Description">

                                </textarea>
                                @error('description')
                                    <div class="text-red-600 mt-2">{{ $message }}</div>
                                @enderror
                            </div>
                            <button type="submit" class="py-3 px-5 w-full text-base font-medium text-center text-white bg-gradient-to-br from-pink-500 to-voilet-500 hover:scale-[1.02] shadow-md shadow-gray-300 transition-transform rounded-lg sm:w-auto">
                                Create
                            </button>
                        </div>
                    </form>
                </div>

            </div>
        </div>
    </div>
</div>
@push('cms.js')
    <script type="module">
        $(function () {
            window.createTasksComponent = @this;
        });
    </script>
    @vite(['resources/assets/cms/js/livewire/tasks/list/create.js'])
@endpush
