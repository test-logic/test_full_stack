<div>
    {{-- Care about people's approval and you will be their prisoner. --}}
    <div class="flex flex-col my-6 mx-4 rounded-2xl shadow-xl shadow-gray-200">
        <button class="bg-blue-500 hover:bg-blue-700 text-white font-bold py-2 px-4 rounded w-fit" wire:click="create">
            Create
        </button>
        <div class="overflow-x-auto rounded-2xl">
           <div class="inline-block min-w-full align-middle">
              <div class="overflow-hidden">
                    
                    <table class="min-w-full divide-y divide-gray-200 table-fixed">
                        <thead class="bg-white">
                        <tr>
                            <th scope="col" class="p-4 text-xs font-medium text-center text-gray-500 uppercase lg:p-5">
                                Action
                            </th>
                            <th scope="col" class="p-4 text-xs font-medium text-center text-gray-500 uppercase lg:p-5">
                                Tasks Name
                            </th>
                            <th scope="col" class="p-4 text-xs font-medium text-center text-gray-500 uppercase lg:p-5">
                                Description
                            </th>
                            <th scope="col" class="p-4 text-xs font-medium text-center text-gray-500 uppercase lg:p-5">
                                Status
                            </th>
                            <th scope="col" class="p-4 text-xs font-medium text-center text-gray-500 uppercase lg:p-5">
                                Created At
                            </th>
                        </tr>
                        </thead>
                        <tbody class="bg-white divide-y divide-gray-200">
                            @if ( isset($tasks) )
                                @foreach($tasks as $task)
                                    <tr class="hover:bg-gray-100">
                                        <td class="p-4 text-sm font-medium text-gray-900 whitespace-nowrap lg:p-5"><a wire:click="edit('{{ $task->task_id }}')">{!! PENTO_SQUARE !!}</a></td>
                                        <td class="p-4 text-sm font-medium text-gray-900 whitespace-nowrap lg:p-5">{{ $task->task_name }}</td>
                                        <td class="p-4 text-sm font-medium text-gray-900 whitespace-nowrap lg:p-5">
                                            <textarea class="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full disabled" disabled rows="3" cols="30">{{ $task->description }}</textarea>
                                        </td>
                                        <td class="p-4 text-sm font-medium text-gray-900 whitespace-nowrap lg:p-5">{{ $task->status }}</td>
                                        <td class="p-4 text-sm font-medium text-gray-900 whitespace-nowrap lg:p-5">{{ $task->created_at }}</td>
                                    </tr>
                                @endforeach
                            @else
                                <tr class="text-center hover:bg-gray-100">
                                    <td colspan="3" class="p-4 text-base font-medium text-gray-900 whitespace-nowrap lg:p-5">
                                        {{ __("No data") }}
                                    </td>
                                </tr>
                            @endif
                        </tbody>
                    </table>
                    
                    @if (!empty($tasks))
                        <div class="p-5">
                            {{ $tasks->links() }}
                        </div>
                    @endif
              </div>
           </div>
        </div>
    </div>
</div>
