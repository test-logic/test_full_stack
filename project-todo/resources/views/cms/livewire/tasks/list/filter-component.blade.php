<div>
    {{-- A good traveler has no fixed plans and is not intent upon arriving. --}}
    <div class="block justify-between items-center p-4 mx-4 mt-1 mb-6 bg-white rounded-2xl shadow-xl shadow-gray-200 lg:p-5 sm:flex">
        <div class="mb-1 w-full">
            <div class="block items-center md:divide-x md:divide-gray-100">
                <form wire:submit="previewData"  class="sm:pr-3 sm:mb-0">
                    <div class="grid grid-cols-1 md:grid-cols-2 md:gap-2 xl:grid-cols-4 xl:gap-4">
                        {{-- <div class="relative mb-2 lg:mb-1">
                            <input type="text" wire:model="phoneSearch" id="products-search" class="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full" placeholder="{{ __("Enter your phone number") }}">
                        </div> --}}
                        <div class="relative mb-2 lg:mb-1 flex">
                            <span class="border border-gray-300 rounded-lg border-e-0 rounded-e-none p-1.5 px-3.5">
                                {!! CALENDAR_DAYS_ICON !!}
                            </span>
                            <input type="text" wire:model="dateRange" id="date-search" class="border rounded-s-none border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full" placeholder="{{ __("Control time") }}">
                        </div>
                        {{-- <div class="flex justify-between mb-2 lg:mb-1 items-center">
                            <label for="show-full-phone" class="flex relative items-center cursor-pointer">
                                <input type="checkbox" wire:model="showPhone" id="show-full-phone" class="sr-only" checked="">
                                <span class="w-11 h-6 bg-gray-200 rounded-full border-2 border-gray-200 toggle-bg mr-1"></span>
                                <span class="text-sm text-base font-semibold text-gray-900">{{ __("Show full phone number") }}</span>
                            </label>
                        </div>
                        <div class="flex mb-2 lg:mb-1 items-center">
                            <label for="show-full-data" class="flex relative items-center cursor-pointer">
                                <input type="checkbox" wire:model="showFullData" id="show-full-data" class="sr-only" checked="">
                                <span class="w-11 h-6 bg-gray-200 rounded-full border-2 border-gray-200 toggle-bg mr-1"></span>
                            </label>
                            <span class="text-sm text-base font-semibold text-gray-900">{{ __("Show full data") }}</span>
                        </div> --}}
                    </div>
                    {{-- <div class="mb-1 py-1 lg:px-0" role="button"
                        data-te-collapse-init
                        data-te-ripple-init
                        data-te-ripple-color="light"
                        data-te-target="#collapseExample"
                        aria-expanded="false"
                        aria-controls="collapseExample">
                        <label class="inline-block text-xs font-medium uppercase leading-normal transition duration-150 ease-in-out">
                            <span>{{ __("Advanced search") }}</span>
                            {!! CHEVRON_DOWN_ICON !!}
                        </label>
                    </div>
                    <div class="grid grid-cols-1 md:grid-cols-2 md:gap-2 xl:grid-cols-4 xl:gap-4 !visible hidden" id="collapseExample" data-te-collapse-item wire:ignore>
                        <div class="relative mb-2 lg:mb-1">
                            <input type="text" wire:model="cmcSearch" class="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full" placeholder="{{ __("Enter CMC") }}">
                        </div>
                        <div class="relative mb-2 lg:mb-1">
                            <input type="text" wire:model="codeSearch" class="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full" placeholder="{{ __("Enter Code") }}">
                        </div>
                        <div class="relative mb-2 lg:mb-1">
                            <input type="text" wire:model="brandName" class="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full" placeholder="{{ __("Enter First Number") }}">
                        </div>
                        <div class="relative mb-2 lg:mb-1">
                            <input type="text" wire:model="cmcSubSearch" class="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full" placeholder="{{ __("Enter CMC Sub") }}">
                        </div>
                        <div class="relative mb-2 lg:mb-1">
                            <input type="text" wire:model="code2Search" class="border border-gray-300 text-gray-900 text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full" placeholder="{{ __("Enter Code2") }}">
                        </div>
                    </div> --}}
                    <div class="w-full mt-4">
                        <button type="submit" class="inline-flex items-center py-2 px-3 text-sm font-medium text-center text-white rounded-lg bg-gradient-to-br from-pink-500 to-voilet-500 sm:ml-auto shadow-md shadow-gray-300 hover:scale-[1.02] transition-transform">
                            {!! MAGNIFYING_GLASS_ICON !!}
                            <span class="ml-1">{{ __('Search') }}</span>
                        </button>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@push('cms.js')
    <script type="module">
        $(function () {
            window.wireFilter = @this;
        });
    </script>
@endpush
