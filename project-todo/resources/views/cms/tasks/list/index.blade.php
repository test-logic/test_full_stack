@extends('cms.masterLayout')

@section('cms.title')
    SMS Short Code Report
@endsection

@section('cms.navigationTop')
    @livewire('partials.navigation-top')
@endsection

@section('cms.slideBar')
    @livewire('partials.slide-bar')
@endsection

@section('cms.content')
    @livewire('tasks.list.filter-component')
    @livewire('tasks.list.list-component')
    @livewire('tasks.list.create-component')
    @livewire('tasks.list.edit-component')
@endsection

@push('cms.js')
    @vite(['resources/assets/cms/js/livewire/tasks/list/index.js'])
@endpush
