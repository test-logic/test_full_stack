@extends('app')

@section('title')
    Create
@endsection

@push('css')
    {{-- <link rel="icon" href="{{ Vite::asset('resources/cms/images/icons/favicon.ico') }}" type="image/vnd.microsoft.icon"> --}}
    @vite(['resources/assets/cms/css/app.css'])
@endpush

@section('content')
<body class="bg-gray-50" data-new-gr-c-s-check-loaded="14.1122.0" data-gr-ext-installed="">
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THQTXJ7" height="0" width="0" style="display: none; visibility: hidden;"></iframe></noscript>
    <main class="bg-gray-50">
        <div class="flex flex-col justify-center items-center px-6 pt-8 mx-auto md:h-screen pt:mt-0">
            {{-- <p class="flex justify-center items-center mb-8 text-2xl font-semibold lg:mb-10">
                <img src="{{ asset('images/icons/logo.svg') }}" class="" alt="Creative Tim Logo" />
                <span class="self-center text-2xl font-bold whitespace-nowrap">Soft UI Dashboard</span>
            </p> --}}

            <p class="px-6 pt-8 mx-auto text-2xl font-semibold mb-5 lg:mb-10">
                {{-- <img src="{{ Vite::asset('resources/cms/images/icons/logo.svg') }}" class="tw-w-auto tw-max-h-8 xl:tw-max-h-12"/> --}}
                <span class="country-indicator tw-relative text-xs tw-text-gray-500 tw-leading-3 float-right">Việt Nam</span>
            </p>

            <div class="p-10 w-full max-w-3xl bg-white rounded-2xl shadow-xl shadow-gray-300">
                <div class="space-y-8">
                    <h2 class="text-2xl font-bold text-gray-900">
                        Create to start your session
                    </h2>
                    <form class="mt-8 grid grid-cols-2 gap-2" action="{{ route('cms.create.post') }}" method="POST">
                        @csrf
                        <div class="relative">
                            <label for="first_name" class="block mb-2 text-sm font-medium text-gray-900">First Name:</label>
                            <input type="text" name="first_name" id="first_name" class="border border-gray-300 @error('first_name') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="First Name" value="{{ old('first_name') }}"/>
                            @error('first_name')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="relative">
                            <label for="last_name" class="block mb-2 text-sm font-medium text-gray-900">Last Name:</label>
                            <input type="text" name="last_name" id="last_name" class="border border-gray-300 @error('last_name') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Last Name" value="{{ old('last_name') }}"/>
                            @error('last_name')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="full_name" class="block mb-2 text-sm font-medium text-gray-900">Full Name:</label>
                            <input type="text" name="full_name" id="full_name" value="{{ old('full_name') }}" class="border border-gray-300 @error('full_name') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Full Name"/>
                            @error('full_name')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="email" class="block mb-2 text-sm font-medium text-gray-900">Email:</label>
                            <input type="text" name="email" id="email" value="{{ old('email') }}" class="border border-gray-300 @error('email') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Email"/>
                            @error('email')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="username" class="block mb-2 text-sm font-medium text-gray-900">Username:</label>
                            <input type="text" name="username" id="username" value="{{ old('username') }}" class="border border-gray-300 @error('username') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Username"/>
                            @error('username')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="password" class="block mb-2 text-sm font-medium text-gray-900">Password:</label>
                            <input type="password" name="password" id="password" value="{{ old('password') }}" class="border border-gray-300 @error('password') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Password"/>
                            @error('password')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <div>
                            <label for="confirn_password" class="block mb-2 text-sm font-medium text-gray-900">Confirn Password:</label>
                            <input type="password" name="confirn_password" id="confirn_password" class="border border-gray-300 @error('confirn_password') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Password"/>
                            @error('confirn_password')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <br>
                        {{-- <div class="flex items-start">
                            <div class="flex items-center h-5">
                                <input id="remember" aria-describedby="remember" name="remember" type="checkbox" class="w-5 h-5 rounded border-gray-300 focus:outline-none focus:ring-0 checked:bg-dark-900" required="" />
                            </div>
                            <div class="ml-3 text-sm">
                                <label for="remember" class="font-medium text-gray-900">Remember me</label>
                            </div>
                            <a href ="#" class="ml-auto text-sm text-fuchsia-600 hover:underline">Lost Password?</a>
                        </div> --}}
                        <button type="submit" class="py-3 px-5 w-full text-base font-medium text-center text-white bg-gradient-to-br from-pink-500 to-voilet-500 hover:scale-[1.02] shadow-md shadow-gray-300 transition-transform rounded-lg sm:w-auto">
                            Create
                        </button>
                        <div class="text-sm font-medium text-gray-500">
                            <a href="{{ route('cms.login') }}" class="text-fuchsia-600 hover:underline">Login account</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</body>

@endsection

{{-- @push('js')
  @vite(['resources/cms/js/app.js'])
@endpush --}}
