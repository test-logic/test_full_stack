@extends('app')

@section('title')
    Login | CMS Syngenta
@endsection

@push('css')
    {{-- <link rel="icon" href="{{ Vite::asset('resources/cms/images/icons/favicon.ico') }}" type="image/vnd.microsoft.icon"> --}}
    @vite(['resources/assets/cms/css/app.css'])
@endpush

@section('content')
<body class="bg-gray-50" data-new-gr-c-s-check-loaded="14.1122.0" data-gr-ext-installed="">
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-THQTXJ7" height="0" width="0" style="display: none; visibility: hidden;"></iframe></noscript>
    <main class="bg-gray-50">
        <div class="flex flex-col justify-center items-center px-6 pt-8 mx-auto md:h-screen pt:mt-0">
            {{-- <p class="flex justify-center items-center mb-8 text-2xl font-semibold lg:mb-10">
                <img src="{{ asset('images/icons/logo.svg') }}" class="" alt="Creative Tim Logo" />
                <span class="self-center text-2xl font-bold whitespace-nowrap">Soft UI Dashboard</span>
            </p> --}}

            <p class="px-6 pt-8 mx-auto text-2xl font-semibold mb-5 lg:mb-10">
                {{-- <img src="{{ Vite::asset('resources/cms/images/icons/logo.svg') }}" class="tw-w-auto tw-max-h-8 xl:tw-max-h-12"/> --}}
                <span class="country-indicator tw-relative text-xs tw-text-gray-500 tw-leading-3 float-right">Việt Nam</span>
            </p>

            <div class="p-10 w-full max-w-lg bg-white rounded-2xl shadow-xl shadow-gray-300">
                <div class="space-y-8">
                    <h2 class="text-2xl font-bold text-gray-900">
                        Sign in to start your session
                    </h2>
                    <form class="mt-8 space-y-6" action="{{ route('cms.login.post') }}" method="POST">
                        @csrf
                        <div>
                            <label for="username" class="block mb-2 text-sm font-medium text-gray-900">Username:</label>
                            <input type="text" name="username" id="username" class="border border-gray-300 @error('username') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Username"/>
                        </div>
                        <div>
                            <label for="password" class="block mb-2 text-sm font-medium text-gray-900">Password:</label>
                            <input type="password" name="password" id="password" class="border border-gray-300 @error('username') border-red-600 @enderror text-gray-900 sm:text-sm rounded-lg focus:ring-2 focus:ring-fuchsia-50 focus:border-fuchsia-300 block w-full p-2.5" placeholder="Password"/>
                            @error('message')
                                <div class="text-red-600 mt-2">{{ $message }}</div>
                            @enderror
                        </div>
                        <div class="flex items-start">
                            <div class="flex items-center h-5">
                                <input id="remember" aria-describedby="remember" name="remember" type="checkbox" class="w-5 h-5 rounded border-gray-300 focus:outline-none focus:ring-0 checked:bg-dark-900" value="true"/>
                            </div>
                            <div class="ml-3 text-sm">
                                <label for="remember" class="font-medium text-gray-900">Remember me</label>
                            </div>
                            {{-- <a href ="#" class="ml-auto text-sm text-fuchsia-600 hover:underline">Lost Password?</a> --}}
                        </div>
                        <button type="submit" class="py-3 px-5 w-full text-base font-medium text-center text-white bg-gradient-to-br from-pink-500 to-voilet-500 hover:scale-[1.02] shadow-md shadow-gray-300 transition-transform rounded-lg sm:w-auto">
                            Login
                        </button>
                        <div class="text-sm font-medium text-gray-500">
                            Not registered?
                            <a href="{{ route('cms.create') }}" class="text-fuchsia-600 hover:underline">Create account</a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </main>
</body>

@endsection

{{-- @push('js')
  @vite(['resources/cms/js/app.js'])
@endpush --}}
