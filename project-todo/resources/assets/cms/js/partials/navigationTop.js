$(function() {
    $("#user-menu-button-2").on('click', function () {
        var toggle = $(this), 
            dropdown2 = $('#dropdown-2');

        if (toggle.attr('aria-expanded') == 'true') {
            dropdown2.removeClass("hidden").toggle(500);

            toggle.attr('aria-expanded', false);
        } else {
            dropdown2.addClass('hidden').toggle(500);

            toggle.attr('aria-expanded', true);
        }
    });
});