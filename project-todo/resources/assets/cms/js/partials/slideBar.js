$(function() {
    // You can use $ inside the wrapper
    $("body").on('click', '#toggleSidebar', function () {
        var toggle = $(this), 
            sidebar = $("#sidebar");
            
        sidebar.removeAttr('style');
        if (toggle.attr('aria-expanded') == 'true') {

            $("#sidebar-items").attr('class', 'flex-1 bg-gray-50 pr-3 pl-1.5');
            sidebar.removeClass('w-64').addClass('w-16');
            // $("[sidebar-toggle-item]").addClass('lg:hidden lg:absolute');
            $("[sidebar-toggle-item]").addClass('!m-0');
            // $("[sidebar-toggle-list]").find('~ [sidebar-toggle-item]').removeClass('ml-3');
            $("[sidebar-bottom-menu]").removeClass('space-x-4 space-y-4 flex-col p-2 p-4').addClass('space-y-4 flex-col p-2');
            $("#main-content").removeClass('lg:ml-64').addClass('lg:ml-16');

            $("[sidebar-toggle-list]").find('li a').addClass('justify-center');
            $("[sidebar-toggle-list]").find('li span').each(function (value) {
                var _this = $(this);
                if (_this.hasClass('hidden')) {
                    _this.removeClass('hidden');
                } else {
                    _this.addClass('hidden');
                }
            });
            toggle.attr('aria-expanded', false);
        } else {

            $("#sidebar-items").attr('class', 'flex-1 bg-gray-50 px-3');
            sidebar.removeClass('w-16').addClass('w-64');
            // $("[sidebar-toggle-item]").removeClass('lg:hidden lg:absolute');
            $("[sidebar-toggle-item]").removeClass('!m-0');
            $("[sidebar-bottom-menu]").removeClass('space-y-4 space-y-4 flex-col p-2 p-4').addClass('space-x-4 p-4');
            $("#main-content").removeClass('lg:ml-16').addClass('lg:ml-64');

            $("[sidebar-toggle-list]").find('li a').removeClass('justify-center');
            $("[sidebar-toggle-list]").find('li span').each(function (value) {
                var _this = $(this);
                if (_this.hasClass('hidden')) {
                    _this.removeClass('hidden');
                } else {
                    _this.addClass('hidden');
                }
            });

            toggle.attr('aria-expanded', true);
        }

        
    });

    $("body").on('click', '#toggleSidebarMobile', function () {
        var toggle = $(this),
            sidebar = $("#sidebar");

        $("#sidebar-items").attr('class', 'flex-1 bg-gray-50 px-3');
        sidebar.removeClass('w-16').addClass('w-64');
        // $("[sidebar-toggle-item]").removeClass('lg:hidden lg:absolute');
        $("[sidebar-toggle-item]").removeClass('!m-0');

        $("[sidebar-bottom-menu]").removeClass('space-y-4 space-y-4 flex-col p-2 p-4').addClass('space-x-4 p-4');
        $("#main-content").removeClass('lg:ml-16').addClass('lg:ml-64');
        
        // thu nhỏ các item
        $("[sidebar-toggle-list]").find('li a').removeClass('justify-center');
        $("[sidebar-toggle-list]").find('li span').each(function (value) {
            var _this = $(this);
            if (_this.hasClass('hidden')) {
                _this.removeClass('hidden');
            } else {
                _this.addClass('hidden');
            }
        });

        // mở to các item
        $("[sidebar-toggle-list]").find('li a').removeClass('justify-center');
        $("[sidebar-toggle-list]").find('li span').each(function (value) {
            var _this = $(this);
            if (_this.hasClass('hidden')) {
                _this.removeClass('hidden');
            } else {
                _this.addClass('hidden');
            }
        });

        // $("#toggleSidebar").attr('aria-expanded', true);
        
        if (toggle.attr('aria-expanded') == 'true') {
            sidebar.toggle(500, "linear", function () {
                $("#toggleSidebarMobileHamburger").addClass('hidden');
                $("#toggleSidebarMobileClose").removeClass('hidden');
                $(this).removeClass('hidden').removeAttr('style');
            });

            toggle.attr('aria-expanded', false);
        } else {
            sidebar.toggle(500, "linear", function () {
                $("#toggleSidebarMobileClose").addClass('hidden');
                $("#toggleSidebarMobileHamburger").removeClass('hidden');
                $(this).addClass('hidden').removeAttr('style');
            });

            toggle.attr('aria-expanded', true);
        }
    });

    $("body").on('click', '[sidebar-toggle-collapse]', function () {
        var toggle = $(this);

        // if (toggle.attr('sidebar-toggle-collapse') == 'true') {
        //     toggle.find("svg[sidebar-toggle-item]").addClass('-rotate-180').removeClass('rotate-0');
        //     console.log(toggle.find("~ [sidebar-toggle-list]"));
        //     toggle.find("~ [sidebar-toggle-list]").slideDown(500, function () {
        //         $(this).removeAttr('style').removeClass('hidden');
        //     });

        //     toggle.attr('sidebar-toggle-collapse', false);
        // } else {
        //     toggle.find("svg[sidebar-toggle-item]").removeClass('-rotate-180').addClass('rotate-0');
        //     toggle.find("~ [sidebar-toggle-list]").slideUp(500, function () {
        //         $(this).removeAttr('style').addClass('hidden');
        //     });

        //     toggle.attr('sidebar-toggle-collapse', true);
        // }
        if (toggle.find("~ [sidebar-toggle-list]").hasClass('hidden')) {
            toggle.find("svg[sidebar-toggle-item]").addClass('-rotate-180').removeClass('rotate-0');
            toggle.find("~ [sidebar-toggle-list]").slideDown(500, function () {
                $(this).removeAttr('style').removeClass('hidden');
            });
        } else {
            toggle.find("svg[sidebar-toggle-item]").removeClass('-rotate-180').addClass('rotate-0');
            toggle.find("~ [sidebar-toggle-list]").slideUp(500, function () {
                $(this).removeAttr('style').addClass('hidden');
            });
        }
    });

    $("body").on('click', '.buyPackage', function () {
        let package_name = $(this).data('package');
        const confirmText = "Bạn mua gói này?";
        if(confirm(confirmText)) {
            Livewire.dispatch('buyPackage', {package_name: package_name});
         }
    });
    
    

    // wire:click="buyPackage('{{ $value->package_name }}')" 
});

