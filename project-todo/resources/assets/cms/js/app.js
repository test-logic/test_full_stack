// import './bootstrap';

import.meta.glob([
    '../images/**',
    '../fonts/**',
]);

import "@fortawesome";

import {
    Datepicker,
    Input,
    Collapse,
    LazyLoad,
    Modal,
    Ripple,
    Toast,
    initTE,
} from "tw-elements";
  
initTE({ Datepicker, Input, Collapse, LazyLoad, Modal, Ripple, Toast });
window.Modal = Modal;

import $ from 'jquery';
window.$ = window.jquery = window.Jquery = $;

import moment from "moment";
window.moment = moment;

import "daterangepicker";

import { Livewire, Alpine } from './../../../../vendor/livewire/livewire/dist/livewire.esm';
import Clipboard from '@ryangjchandler/alpine-clipboard'
 
Alpine.plugin(Clipboard)
 
Livewire.start()

import toastr from "toastr"
window.toastr = toastr;

toastr.options = {
    "closeButton": false,
    "debug": false,
    "newestOnTop": false,
    "progressBar": false,
    "positionClass": "toast-top-right",
    "preventDuplicates": false,
    "onclick": null,
    "showDuration": "300",
    "hideDuration": "1000",
    "timeOut": "5000",
    "extendedTimeOut": "1000",
    "showEasing": "swing",
    "hideEasing": "linear",
    "showMethod": "fadeIn",
    "hideMethod": "fadeOut",
}

import Swal from 'sweetalert2'
window.Swal = Swal;

import select2 from 'select2';
select2();
window.select2 = select2;

import "./master"
