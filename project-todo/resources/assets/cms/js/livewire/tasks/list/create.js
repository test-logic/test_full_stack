$(function () {
    const TasksCreate = document.getElementById('TasksCreate');
    const TasksCreateModal = new Modal(TasksCreate, {
        backdrop: "static",
    });

    const TasksEdit = document.getElementById('TasksEdit');
    const TasksEditModal = new Modal(TasksEdit, {
        backdrop: "static",
    });
    
    
    Livewire.on('openModalCreate', function () {
        TasksCreateModal.show();
    });

    Livewire.on('openModalEdit', function () {
        TasksEditModal.show();
    });
    
});