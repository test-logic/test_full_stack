import './create'

$(function () {
    let minDate = moment().subtract(2, 'months').format('YYYY/MM/01');
    let maxDate = moment().format('YYYY/MM/D');
    $("#date-search").daterangepicker({
        "locale": {
            "format": "YYYY/MM/DD",
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",
            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
        },
        minDate: minDate,
        maxDate: maxDate,
        startDate: maxDate,
        opens: 'left',
        autoApply: true
    });
    $("#date-search").on('change', function (e) {
        var dateRange = $(this).val();

        wireFilter.$set('dateRange', dateRange, false);
    });

    Livewire.hook('commit', ({ component, commit, respond, succeed, fail }) => {
        // Runs immediately before a commit's payload is sent to the server...
        if (
            commit.calls[0] !== undefined &&
            (
                commit.calls[0].method === "previewData" ||
                commit.calls[0].method === "gotoPage" ||
                (commit.calls[0].method === "__dispatch" && commit.calls[0].params[0] === "postTwowayReportList")
            )
        ) {
            Livewire.dispatch('startLoading');
        }

        //
        respond(() => {
            // Runs after a response is received but before it's processed...
            /*
            if ( 
                commit.calls[0] !== undefined &&
                (
                    commit.calls[0].method === "previewData" ||
                    (commit.calls[0].method === "__dispatch" && commit.calls[0].params[0] === "postTwowayReportList")
                )
            ) {
                Livewire.dispatch('startLoading');
            }
            */
        })
     
        succeed(({ snapshot, effect }) => {
            // Runs after a successful response is received and processed
            // with a new snapshot and list of effects...
            if ( 
                commit.calls[0] !== undefined &&
                (
                    commit.calls[0].method === "gotoPage" ||
                    (commit.calls[0].method === "__dispatch" && commit.calls[0].params[0] === "postTasksReportList")
                )
            ) {
                Livewire.dispatch('endLoading');
            }
        })
     
        fail(() => {
            // Runs if some part of the request failed...
            if (
                commit.calls[0] !== undefined &&
                (
                    commit.calls[0].method === "previewData" ||
                    commit.calls[0].method === "gotoPage" ||
                    (commit.calls[0].method === "__dispatch" && commit.calls[0].params[0] === "postTasksReportList")
                )
            ) {
                Livewire.dispatch('endLoading');
            }
        })
    });
});