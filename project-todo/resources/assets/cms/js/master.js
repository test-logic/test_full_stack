Livewire.on('startLoading', function () {
    $('.scene-box').removeClass('!hidden');
});

Livewire.on('endLoading', function () {
    setTimeout(function () {
        $('.scene-box').addClass('!hidden');
    }, 700);
});

Livewire.on('fireSwal', data => {
    let icon = data.type ? data.type : 'info';
    let title = data.title ? data.title : '';
    let text = data.text ? data.text : '';

    Swal.fire({
      icon:  icon,
      title: title,
      html: text
    });
});

Livewire.on('justAlert', (alert) => {
    let type = alert.type;
    switch (type) {
        case 'success':
            toastr.success(alert.type, alert.message);
            break;
        case 'error':
            toastr.error(alert.type, alert.message);
            break;
        case 'warning':
            toastr.warning(alert.type, alert.message);
            break;

        default:
            break;
    }
});

Livewire.on('closeModal', modal_id => {
    $(`#${modal_id}`).find("[data-te-modal-dismiss]").click();
});

Livewire.hook('beforeDomUpdate', () => {
    // Thực hiện xử lý middleware ở đây
    // Ví dụ: Kiểm tra xác thực người dùng trước khi mỗi yêu cầu Livewire được xử lý
    // Nếu người dùng chưa đăng nhập, bạn có thể chuyển hướng họ đến trang đăng nhập
});