<?php
use App\Enums\Packages;

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Cms\Users\Create\CreateController;
use App\Http\Controllers\Cms\Users\Login\LoginController;
use App\Http\Controllers\Cms\Users\Logout\LogoutController;


use App\Http\Controllers\Cms\Tasks\List\TasksListController;

use App\Http\Controllers\Cms\Home\HomeController;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    // return view('welcome');
    return redirect('cms');
});

Route::get('/cms/login',[LoginController::class, 'index'])->name('cms.login');
Route::post('/cms/login',[LoginController::class, 'login'])->name('cms.login.post');

Route::get('/cms/create',[CreateController::class, 'index'])->name('cms.create');
Route::post('/cms/create',[CreateController::class, 'create'])->name('cms.create.post');

Route::get('/cms', function () {
})->middleware(['guest:cms', 'auth.packages']);

Route::prefix('cms')->middleware(['guest:cms'])->name('cms.')->group(function ($route) {

    Route::get('{package}', [HomeController::class, 'index'])->name('home');
    Route::get('{package}/list', [TasksListController::class, 'index'])->name('list');
    
    Route::any('{package}/logout', [LogoutController::class, 'index'])->name('logout');
//     // Route::get('', function () {
//     //     return 123;
//     // })->name('home');
//     // Route::middleware([])->group(function () {
//     //     // dd(request()->all());
        
//     // });

});

// Route::prefix('cms')->middleware('auth.packages')->name('cms.')->group(function () {
    
// });