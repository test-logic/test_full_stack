<?php

    $string = "kkkktttrrrrrrrrrr";
    // $string = "p555ppp7www";
    $groups = [];
    $currentGroup = '';

    for ($i = 0; $i < strlen($string); $i++) {
        if ($i == 0 || $string[$i] != $string[$i - 1]) {
            // Bắt đầu một nhóm mới
            $currentGroup = $string[$i];
        } else {
            // Nối ký tự vào nhóm hiện tại
            $currentGroup .= $string[$i];
        }

        // Nếu ký tự tiếp theo khác ký tự hiện tại, kết thúc nhóm và thêm vào mảng
        if ($i == strlen($string) - 1 || $string[$i] != $string[$i + 1]) {
            $groups[] = $currentGroup;
        }
    }
    $result = implode('', array_map(function($char) {
        return substr($char, 0, 1) . strlen($char);
    }, $groups));

    echo $result;